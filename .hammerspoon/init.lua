last_presses = {}
-- 4k monitor side by side
-- browser_width = 1504

-- Home monitor
browser_width = 1148

-- Work monitor 3rds
-- browser_width = 1003

function withFrame(fun)
   local win = hs.window.focusedWindow()
   local frame = win:frame()
   local screen = win:screen()
   local screenFrame = screen:frame()

   fun(frame, screenFrame)

   win:setFrame(frame)
end

function withFrameF(f)
  return function()
     withFrame(f)
  end
end

function withIsDoubleType(name, fun)
   local last_press_t = last_presses[name]
   local now = os.time()
   isDoubleType = last_press_t and now - last_press_t <= 2
   fun(isDoubleType)
   last_presses[name] = now
end

function bindF(key, f)
   hs.hotkey.bind(
      {"cmd", "alt", "ctrl"},
      key,
      withFrameF(
         function(frame, screenFrame)
            withIsDoubleType(
               key,
               function(isDoubleType)
                  f(frame, screenFrame, isDoubleType)
               end
            )
         end
      )
   )
end

bindF(
   "Left",
   function(frame, screenFrame, isDoubleType)

      frame.x = screenFrame.x
      frame.y = screenFrame.y
      frame.w = browser_width
      frame.h = screenFrame.h

   end
)

bindF(
   "Right",
   function(frame, screenFrame, isDoubleType)

      if isDoubleType then

         frame.x = screenFrame.x + screenFrame.w - (browser_width * 2)
         frame.y = screenFrame.y
         frame.w = browser_width * 2
         frame.h = screenFrame.h

      else

         frame.x = screenFrame.x + screenFrame.w - browser_width
         frame.y = screenFrame.y
         frame.w = browser_width
         frame.h = screenFrame.h

      end


   end
)

bindF(
   "Down",
   function(frame, screenFrame, isDoubleType)
         frame.x = screenFrame.x + browser_width
         frame.y = screenFrame.y
         frame.w = browser_width
         frame.h = screenFrame.h
   end
)


bindF(
   "Up",
   function(frame, screenFrame, isDoubleType)
      if not isDoubleType then
         frame.x = screenFrame.x
         frame.y = screenFrame.y
         frame.w = screenFrame.w
         frame.h = screenFrame.h
      else
         frame.x = screenFrame.x + ( screenFrame.w / 4 )
         frame.y = screenFrame.y
         frame.w = screenFrame.w / 2
         frame.h = screenFrame.h
      end
   end
)

hs.hotkey.bind(
   {"cmd", "alt", "ctrl"},
   "o", -- for "other" screen
   function()
      local win = hs.window.focusedWindow()
      local myScreen = win:screen()
      local otherScreen = myScreen:next()
      win:moveToScreen(otherScreen)
   end
)

---------------------------------------------------------------------
-- Unused
---------------------------------------------------------------------

hs.hotkey.bind({"cmd", "alt", "ctrl"}, "W", function()
      hs.notify.new({title="Hammerspoon", informativeText="Hello World!"}):send()
end)

---------------------------------------------------------------------
-- Quick switch programs
---------------------------------------------------------------------
-- from https://medium.com/@jhkuperus/magical-one-handed-app-switcher-personal-productivity-a959c2e9842a

local This = {}

-- Quickly move to and from a specific app
-- (Thanks Teije)

local previousApp = ""

function This.switchToAndFromApp(bundleID)
  local focusedWindow = hs.window.focusedWindow()
  if focusedWindow == nil then
    hs.application.launchOrFocusByBundleID(bundleID)
  elseif focusedWindow:application():bundleID() == bundleID then
    if previousApp == nil then
      hs.window.switcher.nextWindow()
    else
      previousApp:activate()
    end
  else
    previousApp = focusedWindow:application()
    hs.application.launchOrFocusByBundleID(bundleID)
  end
end

local am = hs.application

--

-- Global Application Keyboard Shortcuts
hs.hotkey.bind({"cmd", "ctrl"}, "s", function()
  This.switchToAndFromApp("com.spotify.client") end)
hs.hotkey.bind({"cmd", "ctrl"}, "z", function()
  This.switchToAndFromApp("us.zoom.xos") end)
hs.hotkey.bind({"cmd", "ctrl"}, "m", function()
  This.switchToAndFromApp("org.gnu.Emacs") end)
hs.hotkey.bind({"cmd", "ctrl"}, "o", function()
  This.switchToAndFromApp("com.roam-research.desktop-app") end)
hs.hotkey.bind({"cmd", "ctrl"}, "l", function()
  This.switchToAndFromApp("com.tinyspeck.slackmacgap") end)
hs.hotkey.bind({"cmd", "ctrl"}, "i", function()
  This.switchToAndFromApp("com.googlecode.iterm2") end)
hs.hotkey.bind({"cmd", "ctrl"}, "h", function()
  This.switchToAndFromApp("com.google.Chrome") end)
hs.hotkey.bind({"cmd", "ctrl"}, "f", function()
  This.switchToAndFromApp("com.apple.finder") end)
hs.hotkey.bind({"cmd", "ctrl"}, "`", function()
  print(pcall(minimizeAllButFocussedWindow)) end)
-- show the bundleid of the currently open window
-- hs.hotkey.bind("cmd", "b", function()
--     local bundleid = hs.window.focusedwindow():application():bundleid()
--     hs.alert.show(bundleid)
--     hs.pasteboard.setcontents(bundleid)
-- end)

---------------------------------------------------------------------
-- Tabs
---------------------------------------------------------------------

function chrome_active_tab_with_name(name, url)
    return function()
        hs.osascript.javascript([[
            // below is javascript code
            var chrome = Application('Google Chrome');
            chrome.activate();
            var wins = chrome.windows;
            // loop tabs to find a web page with a title of <name>
            var tabFound = false;
            for (var i = 0; i < wins.length; i++) {
                var win = wins.at(i);
                var tabs = win.tabs;
                for (var j = 0; j < tabs.length; j++) {
                var tab = tabs.at(j);
                tab.title(); j;
                if (tab.title().indexOf(']] .. name .. [[') > -1) {
                        win.activeTabIndex = j + 1;
                        tabFound = true;
                    }
                }
            }
            if (!tabFound) {
              newTab = chrome.Tab();
              chrome.windows[0].tabs.push(newTab);
              newTab.url = ']] .. url .. [[';
            }
            // end of javascript
        ]])
    end
end

hs.hotkey.bind({"cmd", "ctrl"}, "c", chrome_active_tab_with_name("Clubhouse", "https://app.clubhouse.io/kaddy/stories/space/953/dev-focus"))
hs.hotkey.bind({"cmd", "ctrl"}, "g", chrome_active_tab_with_name("Inbox", "https://mail.google.com/mail/u/0/#inbox"))
hs.hotkey.bind({"cmd", "ctrl"}, "a", chrome_active_tab_with_name("Calendar", "https://calendar.google.com/calendar/u/0/r"))
hs.hotkey.bind({"cmd", "ctrl"}, "k", chrome_active_tab_with_name("Kaddy Admin", "http://localhost:3003/orders"))


return This
