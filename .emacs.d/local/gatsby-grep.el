;;; Code:

(defconst gatsby-grep-command
  ;; Excludes tests
  "find ~/dev/gatsby/gatsby/packages \\( -path '*/__tests__/*' -o -path '/*/__testfixtures__/*' -o -path '*/dist/*' -o -path '*/node_modules/*' \\) -prune -o -path '*/src/*' -o -path 'gatsby/cache-dir' -type f -name '*.js' -exec grep -nH -v -e '//' \\{\\} + | grep -v ' \\* ' | grep -e "
  )

(defconst gatsby-grep-command-all
  ;; Excludes tests
  "find ~/dev/gatsby/gatsby/packages \\( -path '*/dist/*' -o -path '*/node_modules/*' \\) -prune -o -path '*/src/*' -o -path 'gatsby/cache-dir' -type f -name '*.js' -exec grep -nH -e \\{\\} +"
  )

;;;###autoload
(defun gatsby-grep (command-args)
  "Recursively greps for js files in gatsby/packages. Only
  searches in package's src dirs and ignores things like
  node_modules, dist, tests etc"
  (interactive
   (list
    (read-shell-command
     "Gatsby grep (do gatsby-*.js seperately): "
     (list (concat gatsby-grep-command (thing-at-point 'word)))
     'grep-find-history)))
  (grep command-args))

;;;###autoload
(defun gatsby-grep-all (command-args)
  "Recursively greps for js files in gatsby/packages. Only
  searches in package's src dirs and ignores things like
  node_modules, dist, tests etc"
  (interactive
   (list
    (read-shell-command
     "Gatsby grep (do gatsby-*.js seperately): "
     (list (concat gatsby-grep-command-all (thing-at-point 'word)))
     'grep-find-history)))
  (grep command-args))

(provide 'gatsby-grep)
(provide 'gatsby-grep-all)

;;; gatsby-grep.el ends here
