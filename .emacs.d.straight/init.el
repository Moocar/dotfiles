(setq debug-on-error t)
(require 'ob-tangle)
(require 'org)
(message (org-version))
(org-babel-load-file (expand-file-name "anthony.org" user-emacs-directory))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("8f5a7a9a3c510ef9cbb88e600c0b4c53cdcdb502cfe3eb50040b7e13c6f4e78e" default))
 '(fci-rule-color "#5B6268")
 '(ivy-count-format "(%d/%d) ")
 '(ivy-use-virtual-buffers t)
 '(jdee-db-active-breakpoint-face-colors (cons "#1B2229" "#51afef"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#1B2229" "#98be65"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#1B2229" "#3f444a"))
 '(objed-cursor-color "#ff6c6b")
 '(pdf-view-midnight-colors (cons "#bbc2cf" "#282c34"))
 '(projectile-completion-system 'ivy)
 '(rustic-ansi-faces
   ["#282c34" "#ff6c6b" "#98be65" "#ECBE7B" "#51afef" "#c678dd" "#46D9FF" "#bbc2cf"])
 '(safe-local-variable-values
   '((>defn
      '(:defn
	(1)))
     (table
      '(1))
     (ui-tab-pane
      '(1))
     (ui-form-select
      '(1))
     (ui-form-input
      '(1))
     (ui-dropdown-menu
      '(1))
     (ui-dropdown-item
      '(1))
     (ui-dropdown
      '(1))
     (ui-modal-actions
      '(1))
     (ui-modal-content
      '(1))
     (ui-modal-header
      '(1))
     (ui-modal
      '(1))
     (ui-list-header
      '(1))
     (ui-list-content
      '(1))
     (ui-list-item
      '(1))
     (ui-list
      '(1))
     (ui-segment
      '(1))
     (ui-table-header
      '(1))
     (ui-table-body
      '(1))
     (ui-table-row
      '(1))
     (ui-table
      '(1))
     (select
      '(1))
     (div
      '(1))
     (lsp--override-calculate-lisp-indent\? . t)
     (flycheck-disabled-checkers quote
				 (emacs-lisp-checkdoc))
     (eval progn
	   (let
	       ((dirloc-lsp-defun-regexp
		 (concat
		  (concat "^\\s-*(" "lsp-defun" "\\s-+\\(")
		  (or
		   (bound-and-true-p lisp-mode-symbol-regexp)
		   "\\(?:\\sw\\|\\s_\\|\\\\.\\)+")
		  "\\)")))
	     (add-to-list 'imenu-generic-expression
			  (list "Functions" dirloc-lsp-defun-regexp 1)))
	   (defvar lsp--override-calculate-lisp-indent\? nil "Whether to override `lisp-indent-function' with
              the updated `calculate-lisp-indent' definition from
              Emacs 28.")
	   (defun wrap-calculate-lisp-indent
	       (func &optional parse-start)
	     "Return appropriate indentation for current line as Lisp code.
In usual case returns an integer: the column to indent to.
If the value is nil, that means don't change the indentation
because the line starts inside a string.

PARSE-START may be a buffer position to start parsing from, or a
parse state as returned by calling `parse-partial-sexp' up to the
beginning of the current line.

The value can also be a list of the form (COLUMN CONTAINING-SEXP-START).
This means that following lines at the same level of indentation
should not necessarily be indented the same as this line.
Then COLUMN is the column to indent to, and CONTAINING-SEXP-START
is the buffer position of the start of the containing expression."
	     (if
		 (not lsp--override-calculate-lisp-indent\?)
		 (funcall func parse-start)
	       (save-excursion
		 (beginning-of-line)
		 (let
		     ((indent-point
		       (point))
		      state
		      (desired-indent nil)
		      (retry t)
		      whitespace-after-open-paren calculate-lisp-indent-last-sexp containing-sexp)
		   (cond
		    ((or
		      (markerp parse-start)
		      (integerp parse-start))
		     (goto-char parse-start))
		    ((null parse-start)
		     (beginning-of-defun))
		    (t
		     (setq state parse-start)))
		   (unless state
		     (while
			 (<
			  (point)
			  indent-point)
		       (setq state
			     (parse-partial-sexp
			      (point)
			      indent-point 0))))
		   (while
		       (and retry state
			    (>
			     (elt state 0)
			     0))
		     (setq retry nil)
		     (setq calculate-lisp-indent-last-sexp
			   (elt state 2))
		     (setq containing-sexp
			   (elt state 1))
		     (goto-char
		      (1+ containing-sexp))
		     (if
			 (and calculate-lisp-indent-last-sexp
			      (> calculate-lisp-indent-last-sexp
				 (point)))
			 (let
			     ((peek
			       (parse-partial-sexp calculate-lisp-indent-last-sexp indent-point 0)))
			   (if
			       (setq retry
				     (car
				      (cdr peek)))
			       (setq state peek)))))
		   (if retry nil
		     (goto-char
		      (1+ containing-sexp))
		     (setq whitespace-after-open-paren
			   (looking-at
			    (rx whitespace)))
		     (if
			 (not calculate-lisp-indent-last-sexp)
			 (setq desired-indent
			       (current-column))
		       (parse-partial-sexp
			(point)
			calculate-lisp-indent-last-sexp 0 t)
		       (cond
			((looking-at "\\s("))
			((>
			  (save-excursion
			    (forward-line 1)
			    (point))
			  calculate-lisp-indent-last-sexp)
			 (if
			     (or
			      (=
			       (point)
			       calculate-lisp-indent-last-sexp)
			      whitespace-after-open-paren)
			     nil
			   (progn
			     (forward-sexp 1)
			     (parse-partial-sexp
			      (point)
			      calculate-lisp-indent-last-sexp 0 t)))
			 (backward-prefix-chars))
			(t
			 (goto-char calculate-lisp-indent-last-sexp)
			 (beginning-of-line)
			 (parse-partial-sexp
			  (point)
			  calculate-lisp-indent-last-sexp 0 t)
			 (backward-prefix-chars)))))
		   (let
		       ((normal-indent
			 (current-column)))
		     (cond
		      ((elt state 3)
		       nil)
		      ((and
			(integerp lisp-indent-offset)
			containing-sexp)
		       (goto-char containing-sexp)
		       (+
			(current-column)
			lisp-indent-offset))
		      (calculate-lisp-indent-last-sexp
		       (or
			(and lisp-indent-function
			     (not retry)
			     (funcall lisp-indent-function indent-point state))
			(and
			 (save-excursion
			   (goto-char indent-point)
			   (skip-chars-forward " 	")
			   (looking-at ":"))
			 (save-excursion
			   (goto-char calculate-lisp-indent-last-sexp)
			   (backward-prefix-chars)
			   (while
			       (not
				(or
				 (looking-back "^[ 	]*\\|([ 	]+"
					       (line-beginning-position))
				 (and containing-sexp
				      (>=
				       (1+ containing-sexp)
				       (point)))))
			     (forward-sexp -1)
			     (backward-prefix-chars))
			   (setq calculate-lisp-indent-last-sexp
				 (point)))
			 (> calculate-lisp-indent-last-sexp
			    (save-excursion
			      (goto-char
			       (1+ containing-sexp))
			      (parse-partial-sexp
			       (point)
			       calculate-lisp-indent-last-sexp 0 t)
			      (point)))
			 (let
			     ((parse-sexp-ignore-comments t)
			      indent)
			   (goto-char calculate-lisp-indent-last-sexp)
			   (or
			    (and
			     (looking-at ":")
			     (setq indent
				   (current-column)))
			    (and
			     (<
			      (line-beginning-position)
			      (prog2
				  (backward-sexp)
				  (point)))
			     (looking-at ":")
			     (setq indent
				   (current-column))))
			   indent))
			normal-indent))
		      (desired-indent)
		      (t normal-indent)))))))
	   (when
	       (< emacs-major-version 28)
	     (advice-add #'calculate-lisp-indent :around #'wrap-calculate-lisp-indent)))
     (cider-clojure-cli-global-options . "-A:server-dev")
     (cider-clojure-cli-parameters . "")
     (cider-clojure-cli-global-options . "-M:inspect/reveal-nrepl-cider:server-dev")
     (cider-jack-in-repl-middlewares)
     (clojure-preferred-build-tool . clojure-cli)
     (projectile-project-test-cmd . "make test")
     (projectile-project-run-cmd . "make run-api")
     (eval define-clojure-indent
	   (div
	    '(1))
	   (select
	    '(1))
	   (ui-table
	    '(1))
	   (ui-table-row
	    '(1))
	   (ui-table-body
	    '(1))
	   (ui-table-header
	    '(1))
	   (ui-segment
	    '(1))
	   (ui-list
	    '(1))
	   (ui-list-item
	    '(1))
	   (ui-list-content
	    '(1))
	   (ui-list-header
	    '(1))
	   (ui-modal
	    '(1))
	   (ui-modal-header
	    '(1))
	   (ui-modal-content
	    '(1))
	   (ui-modal-actions
	    '(1))
	   (ui-dropdown
	    '(1))
	   (ui-dropdown-item
	    '(1))
	   (ui-dropdown-menu
	    '(1))
	   (ui-form
	    '(1))
	   (ui-form-input
	    '(1))
	   (ui-form-select
	    '(1))
	   (ui-tab-pane
	    '(1))
	   (table
	    '(1))
	   (>defn
	    '(:defn
	      (1))))
     (cljr-magic-require-namespaces
      ("io" . "clojure.java.io")
      ("set" . "clojure.set")
      ("str" . "clojure.string")
      ("walk" . "clojure.walk")
      ("edn" . "clojure.edn")
      ("zip" . "clojure.zip")
      ("async" . "clojure.core.async")
      ("s" . "clojure.spec.alpha")
      ("instant" . "clojure.instant")
      ("log" . "clojure.tools.logging")
      ("cheshire" . "cheshire.core")
      ("ig" . "integrant.core")
      ("jdbc" . "next.jdbc")
      ("eql" . "edn-query-language.core")
      ("order" . "kaddy.specs.order")
      ("order-event" . "kaddy.specs.order.event")
      ("order-metadata" . "kaddy.specs.order.metadata")
      ("customer" . "kaddy.specs.customer")
      ("supplier" . "kaddy.specs.supplier")
      ("contact" . "kaddy.specs.contact")
      ("address" . "kaddy.specs.address")
      ("product" . "kaddy.specs.product")
      ("shopify" . "kaddy.specs.shopify")
      ("incentive" . "kaddy.specs.incentive")
      ("line-item" . "kaddy.specs.line-item")
      ("region" . "kaddy.specs.region")
      ("user" . "kaddy.specs.user")
      ("supplier-region" . "kaddy.specs.supplier.region")
      ("debit-account" . "kaddy.specs.debit-account")
      ("split-contact" . "kaddy.specs.split.contact")
      ("user" . "kaddy.specs.user")
      ("ks" . "kaddy.specs")
      ("m-order" . "kaddy.model.order")
      ("m-customer" . "kaddy.model.customer")
      ("m-supplier" . "kaddy.model.supplier")
      ("m-contact" . "kaddy.model.contact")
      ("m-address" . "kaddy.model.address")
      ("db" . "kaddy.db")
      ("misc" . "kaddy.misc")
      ("math" . "kaddy.math")
      ("env" . "kaddy.env")
      ("fulcro-util" . "kaddy.app.fulcro-util")
      ("admin-api" . "kaddy.app.admin-api")
      ("tempid" . "com.fulcrologic.fulcro.algorithms.tempid")
      ("fdn" . "com.fulcrologic.fulcro.algorithms.denormalize")
      ("comp" . "com.fulcrologic.fulcro.components")
      ("dom" . "com.fulcrologic.fulcro.dom")
      ("fs" . "com.fulcrologic.fulcro.algorithms.form-state")
      ("df" . "com.fulcrologic.fulcro.data-fetch")
      ("dr" . "com.fulcrologic.fulcro.routing.dynamic-routing")
      ("m" . "com.fulcrologic.fulcro.mutations")
      ("uism" . "com.fulcrologic.fulcro.ui-state-machines")
      ("targeting" . "com.fulcrologic.fulcro.algorithms.data-targeting")
      ("app" . "com.fulcrologic.fulcro.application"))
     (cljr-magic-require-namespaces
      ("io" . "clojure.java.io")
      ("set" . "clojure.set")
      ("str" . "clojure.string")
      ("walk" . "clojure.walk")
      ("edn" . "clojure.edn")
      ("zip" . "clojure.zip")
      ("async" . "clojure.core.async")
      ("s" . "clojure.spec.alpha")
      ("instant" . "clojure.instant")
      ("log" . "clojure.tools.logging")
      ("cheshire" . "cheshire.core")
      ("ig" . "integrant.core")
      ("jdbc" . "next.jdbc")
      ("order" . "kaddy.specs.order")
      ("order-event" . "kaddy.specs.order.event")
      ("order-metadata" . "kaddy.specs.order.metadata")
      ("customer" . "kaddy.specs.customer")
      ("supplier" . "kaddy.specs.supplier")
      ("contact" . "kaddy.specs.contact")
      ("address" . "kaddy.specs.address")
      ("product" . "kaddy.specs.product")
      ("shopify" . "kaddy.specs.shopify")
      ("incentive" . "kaddy.specs.incentive")
      ("line-item" . "kaddy.specs.line-item")
      ("region" . "kaddy.specs.region")
      ("user" . "kaddy.specs.user")
      ("supplier-region" . "kaddy.specs.supplier.region")
      ("debit-account" . "kaddy.specs.debit-account")
      ("split-contact" . "kaddy.specs.split.contact")
      ("user" . "kaddy.specs.user")
      ("ks" . "kaddy.specs")
      ("m-order" . "kaddy.model.order")
      ("m-customer" . "kaddy.model.customer")
      ("m-supplier" . "kaddy.model.supplier")
      ("m-contact" . "kaddy.model.contact")
      ("m-address" . "kaddy.model.address")
      ("db" . "kaddy.db")
      ("misc" . "kaddy.misc")
      ("math" . "kaddy.math")
      ("env" . "kaddy.env")
      ("fulcro-util" . "kaddy.app.fulcro-util")
      ("tempid" . "com.fulcrologic.fulcro.algorithms.tempid")
      ("fdn" . "com.fulcrologic.fulcro.algorithms.denormalize")
      ("comp" . "com.fulcrologic.fulcro.components")
      ("dom" . "com.fulcrologic.fulcro.dom")
      ("fs" . "com.fulcrologic.fulcro.algorithms.form-state")
      ("df" . "com.fulcrologic.fulcro.data-fetch")
      ("dr" . "com.fulcrologic.fulcro.routing.dynamic-routing")
      ("m" . "com.fulcrologic.fulcro.mutations")
      ("uism" . "com.fulcrologic.fulcro.ui-state-machines")
      ("targeting" . "com.fulcrologic.fulcro.algorithms.data-targeting")
      ("app" . "com.fulcrologic.fulcro.application"))
     (cljr-magic-require-namespaces
      ("io" . "clojure.java.io")
      ("set" . "clojure.set")
      ("str" . "clojure.string")
      ("walk" . "clojure.walk")
      ("edn" . "clojure.edn")
      ("zip" . "clojure.zip")
      ("async" . "clojure.core.async")
      ("s" . "clojure.spec.alpha")
      ("instant" . "clojure.instant")
      ("log" . "clojure.tools.logging")
      ("cheshire" . "cheshire.core")
      ("ig" . "integrant.core")
      ("jdbc" . "next.jdbc")
      ("eql" . "edn-query-language.core")
      ("order" . "kaddy.specs.order")
      ("customer" . "kaddy.specs.customer")
      ("supplier" . "kaddy.specs.supplier")
      ("contact" . "kaddy.specs.contact")
      ("address" . "kaddy.specs.address")
      ("product" . "kaddy.specs.product")
      ("shopify" . "kaddy.specs.shopify")
      ("incentive" . "kaddy.specs.incentive")
      ("line-item" . "kaddy.specs.line-item")
      ("region" . "kaddy.specs.region")
      ("user" . "kaddy.specs.user")
      ("supplier-region" . "kaddy.specs.supplier.region")
      ("debit-account" . "kaddy.specs.debit-account")
      ("split-contact" . "kaddy.specs.split.contact")
      ("user" . "kaddy.specs.user")
      ("ks" . "kaddy.specs")
      ("m-order" . "kaddy.model.order")
      ("m-customer" . "kaddy.model.customer")
      ("m-supplier" . "kaddy.model.supplier")
      ("m-contact" . "kaddy.model.contact")
      ("m-address" . "kaddy.model.address")
      ("db" . "kaddy.db")
      ("misc" . "kaddy.misc")
      ("math" . "kaddy.math")
      ("env" . "kaddy.env")
      ("fulcro-util" . "kaddy.app.fulcro-util")
      ("admin-api" . "kaddy.app.admin-api")
      ("tempid" . "com.fulcrologic.fulcro.algorithms.tempid")
      ("fdn" . "com.fulcrologic.fulcro.algorithms.denormalize")
      ("comp" . "com.fulcrologic.fulcro.components")
      ("dom" . "com.fulcrologic.fulcro.dom")
      ("fs" . "com.fulcrologic.fulcro.algorithms.form-state")
      ("df" . "com.fulcrologic.fulcro.data-fetch")
      ("dr" . "com.fulcrologic.fulcro.routing.dynamic-routing")
      ("m" . "com.fulcrologic.fulcro.mutations")
      ("uism" . "com.fulcrologic.fulcro.ui-state-machines")
      ("targeting" . "com.fulcrologic.fulcro.algorithms.data-targeting")
      ("app" . "com.fulcrologic.fulcro.application"))
     (cljr-magic-require-namespaces
      ("io" . "clojure.java.io")
      ("set" . "clojure.set")
      ("str" . "clojure.string")
      ("walk" . "clojure.walk")
      ("edn" . "clojure.edn")
      ("zip" . "clojure.zip")
      ("async" . "clojure.core.async")
      ("s" . "clojure.spec.alpha")
      ("instant" . "clojure.instant")
      ("log" . "clojure.tools.logging")
      ("cheshire" . "cheshire.core")
      ("ig" . "integrant.core")
      ("jdbc" . "next.jdbc")
      ("eql" . "edn-query-language.core")
      ("order" . "kaddy.specs.order")
      ("customer" . "kaddy.specs.customer")
      ("supplier" . "kaddy.specs.supplier")
      ("contact" . "kaddy.specs.contact")
      ("address" . "kaddy.specs.address")
      ("product" . "kaddy.specs.product")
      ("shopify" . "kaddy.specs.shopify")
      ("incentive" . "kaddy.specs.incentive")
      ("line-item" . "kaddy.specs.line-item")
      ("region" . "kaddy.specs.region")
      ("user" . "kaddy.specs.user")
      ("supplier-region" . "kaddy.specs.supplier.region")
      ("debit-account" . "kaddy.specs.debit-account")
      ("split-contact" . "kaddy.specs.split.contact")
      ("ks" . "kaddy.specs")
      ("m-order" . "kaddy.model.order")
      ("m-customer" . "kaddy.model.customer")
      ("m-supplier" . "kaddy.model.supplier")
      ("m-contact" . "kaddy.model.contact")
      ("m-address" . "kaddy.model.address")
      ("db" . "kaddy.db")
      ("misc" . "kaddy.misc")
      ("math" . "kaddy.math")
      ("env" . "kaddy.env")
      ("fulcro-util" . "kaddy.app.fulcro-util")
      ("admin-api" . "kaddy.app.admin-api")
      ("tempid" . "com.fulcrologic.fulcro.algorithms.tempid")
      ("fdn" . "com.fulcrologic.fulcro.algorithms.denormalize")
      ("comp" . "com.fulcrologic.fulcro.components")
      ("dom" . "com.fulcrologic.fulcro.dom")
      ("fs" . "com.fulcrologic.fulcro.algorithms.form-state")
      ("df" . "com.fulcrologic.fulcro.data-fetch")
      ("dr" . "com.fulcrologic.fulcro.routing.dynamic-routing")
      ("m" . "com.fulcrologic.fulcro.mutations")
      ("uism" . "com.fulcrologic.fulcro.ui-state-machines")
      ("targeting" . "com.fulcrologic.fulcro.algorithms.data-targeting")
      ("app" . "com.fulcrologic.fulcro.application"))
     (cljr-magic-require-namespaces
      ("io" . "clojure.java.io")
      ("set" . "clojure.set")
      ("str" . "clojure.string")
      ("walk" . "clojure.walk")
      ("edn" . "clojure.edn")
      ("zip" . "clojure.zip")
      ("async" . "clojure.core.async")
      ("s" . "clojure.spec.alpha")
      ("instant" . "clojure.instant")
      ("log" . "clojure.tools.logging")
      ("cheshire" . "cheshire.core")
      ("ig" . "integrant.core")
      ("jdbc" . "next.jdbc")
      ("eql" . "edn-query-language.core")
      ("order" . "kaddy.specs.order")
      ("customer" . "kaddy.specs.customer")
      ("supplier" . "kaddy.specs.supplier")
      ("contact" . "kaddy.specs.contact")
      ("address" . "kaddy.specs.address")
      ("product" . "kaddy.specs.product")
      ("shopify" . "kaddy.specs.shopify")
      ("incentive" . "kaddy.specs.incentive")
      ("line-item" . "kaddy.specs.line-item")
      ("region" . "kaddy.specs.region")
      ("user" . "kaddy.specs.user")
      ("supplier-region" . "kaddy.specs.supplier.region")
      ("debit-account" . "kaddy.specs.debit-account")
      ("split-contact" . "kaddy.specs.split.contact")
      ("ks" . "kaddy.specs")
      ("m-order" . "kaddy.model.order")
      ("m-customer" . "kaddy.model.customer")
      ("m-supplier" . "kaddy.model.supplier")
      ("m-contact" . "kaddy.model.contact")
      ("m-address" . "kaddy.model.address")
      ("db" . "kaddy.db")
      ("misc" . "kaddy.misc")
      ("math" . "kaddy.math")
      ("env" . "kaddy.env")
      ("fulcro-util" . "kaddy.app.fulcro-util")
      ("tempid" . "com.fulcrologic.fulcro.algorithms.tempid")
      ("fdn" . "com.fulcrologic.fulcro.algorithms.denormalize")
      ("comp" . "com.fulcrologic.fulcro.components")
      ("dom" . "com.fulcrologic.fulcro.dom")
      ("fs" . "com.fulcrologic.fulcro.algorithms.form-state")
      ("df" . "com.fulcrologic.fulcro.data-fetch")
      ("dr" . "com.fulcrologic.fulcro.routing.dynamic-routing")
      ("m" . "com.fulcrologic.fulcro.mutations")
      ("uism" . "com.fulcrologic.fulcro.ui-state-machines")
      ("targeting" . "com.fulcrologic.fulcro.algorithms.data-targeting")
      ("app" . "com.fulcrologic.fulcro.application"))
     (eval define-clojure-indent
	   (div
	    '(1))
	   (select
	    '(1))
	   (ui-table
	    '(1))
	   (ui-table-row
	    '(1))
	   (ui-table-body
	    '(1))
	   (ui-table-header
	    '(1))
	   (ui-segment
	    '(1))
	   (ui-list
	    '(1))
	   (ui-list-item
	    '(1))
	   (ui-list-content
	    '(1))
	   (ui-list-header
	    '(1))
	   (ui-modal
	    '(1))
	   (ui-modal-header
	    '(1))
	   (ui-modal-content
	    '(1))
	   (ui-modal-actions
	    '(1))
	   (ui-form
	    '(1))
	   (ui-form-input
	    '(1))
	   (ui-form-select
	    '(1))
	   (ui-tab-pane
	    '(1))
	   (table
	    '(1))
	   (>defn
	    '(:defn
	      (1))))
     (eval define-clojure-indent
	   (div
	    '(1))
	   (select
	    '(1))
	   (ui-table
	    '(1))
	   (ui-table-row
	    '(1))
	   (ui-table-body
	    '(1))
	   (ui-table-header
	    '(1))
	   (ui-segment
	    '(1))
	   (ui-list
	    '(1))
	   (ui-list-item
	    '(1))
	   (ui-list-content
	    '(1))
	   (ui-list-header
	    '(1))
	   (ui-modal
	    '(1))
	   (ui-modal-header
	    '(1))
	   (ui-modal-content
	    '(1))
	   (ui-modal-actions
	    '(1))
	   (ui-form
	    '(1))
	   (ui-form-input
	    '(1))
	   (ui-form-select
	    '(1))
	   (ui-tab-form
	    '(1))
	   (table
	    '(1))
	   (>defn
	    '(:defn
	      (1))))
     (cljr-magic-require-namespaces
      ("io" . "clojure.java.io")
      ("set" . "clojure.set")
      ("str" . "clojure.string")
      ("walk" . "clojure.walk")
      ("edn" . "clojure.edn")
      ("zip" . "clojure.zip")
      ("async" . "clojure.core.async")
      ("s" . "clojure.spec.alpha")
      ("instant" . "clojure.instant")
      ("log" . "clojure.tools.logging")
      ("cheshire" . "cheshire.core")
      ("ig" . "integrant.core")
      ("jdbc" . "next.jdbc")
      ("order" . "kaddy.specs.order")
      ("customer" . "kaddy.specs.customer")
      ("supplier" . "kaddy.specs.supplier")
      ("contact" . "kaddy.specs.contact")
      ("address" . "kaddy.specs.address")
      ("product" . "kaddy.specs.product")
      ("shopify" . "kaddy.specs.shopify")
      ("incentive" . "kaddy.specs.incentive")
      ("line-item" . "kaddy.specs.line-item")
      ("region" . "kaddy.specs.region")
      ("user" . "kaddy.specs.user")
      ("supplier-region" . "kaddy.specs.supplier.region")
      ("debit-account" . "kaddy.specs.debit-account")
      ("split-contact" . "kaddy.specs.split.contact")
      ("user" . "kaddy.specs.user")
      ("ks" . "kaddy.specs")
      ("m-order" . "kaddy.model.order")
      ("m-customer" . "kaddy.model.customer")
      ("m-supplier" . "kaddy.model.supplier")
      ("m-contact" . "kaddy.model.contact")
      ("m-address" . "kaddy.model.address")
      ("db" . "kaddy.db")
      ("misc" . "kaddy.misc")
      ("math" . "kaddy.math")
      ("env" . "kaddy.env")
      ("fulcro-util" . "kaddy.app.fulcro-util")
      ("tempid" . "com.fulcrologic.fulcro.algorithms.tempid")
      ("fdn" . "com.fulcrologic.fulcro.algorithms.denormalize")
      ("comp" . "com.fulcrologic.fulcro.components")
      ("dom" . "com.fulcrologic.fulcro.dom")
      ("fs" . "com.fulcrologic.fulcro.algorithms.form-state")
      ("df" . "com.fulcrologic.fulcro.data-fetch")
      ("dr" . "com.fulcrologic.fulcro.routing.dynamic-routing")
      ("m" . "com.fulcrologic.fulcro.mutations")
      ("uism" . "com.fulcrologic.fulcro.ui-state-machines")
      ("targeting" . "com.fulcrologic.fulcro.algorithms.data-targeting")
      ("app" . "com.fulcrologic.fulcro.application"))
     (cljr-magic-require-namespaces
      ("io" . "clojure.java.io")
      ("set" . "clojure.set")
      ("str" . "clojure.string")
      ("walk" . "clojure.walk")
      ("edn" . "clojure.edn")
      ("zip" . "clojure.zip")
      ("async" . "clojure.core.async")
      ("s" . "clojure.spec.alpha")
      ("instant" . "clojure.instant")
      ("log" . "clojure.tools.logging")
      ("cheshire" . "cheshire.core")
      ("ig" . "integrant.core")
      ("jdbc" . "next.jdbc")
      ("order" . "kaddy.specs.order")
      ("customer" . "kaddy.specs.customer")
      ("supplier" . "kaddy.specs.supplier")
      ("contact" . "kaddy.specs.contact")
      ("address" . "kaddy.specs.address")
      ("product" . "kaddy.specs.product")
      ("shopify" . "kaddy.specs.shopify")
      ("incentive" . "kaddy.specs.incentive")
      ("line-item" . "kaddy.specs.line-item")
      ("region" . "kaddy.specs.region")
      ("user" . "kaddy.specs.user")
      ("supplier-region" . "kaddy.specs.supplier.region")
      ("debit-account" . "kaddy.specs.debit-account")
      ("split-contact" . "kaddy.specs.split.contact")
      ("ks" . "kaddy.specs")
      ("m-order" . "kaddy.model.order")
      ("m-customer" . "kaddy.model.customer")
      ("m-supplier" . "kaddy.model.supplier")
      ("m-contact" . "kaddy.model.contact")
      ("m-address" . "kaddy.model.address")
      ("db" . "kaddy.db")
      ("misc" . "kaddy.misc")
      ("math" . "kaddy.math")
      ("env" . "kaddy.env")
      ("fulcro-util" . "kaddy.app.fulcro-util")
      ("tempid" . "com.fulcrologic.fulcro.algorithms.tempid")
      ("fdn" . "com.fulcrologic.fulcro.algorithms.denormalize")
      ("comp" . "com.fulcrologic.fulcro.components")
      ("dom" . "com.fulcrologic.fulcro.dom")
      ("fs" . "com.fulcrologic.fulcro.algorithms.form-state")
      ("df" . "com.fulcrologic.fulcro.data-fetch")
      ("dr" . "com.fulcrologic.fulcro.routing.dynamic-routing")
      ("m" . "com.fulcrologic.fulcro.mutations")
      ("uism" . "com.fulcrologic.fulcro.ui-state-machines")
      ("targeting" . "com.fulcrologic.fulcro.algorithms.data-targeting")
      ("app" . "com.fulcrologic.fulcro.application"))
     (cljr-magic-require-namespaces
      ("io" . "clojure.java.io")
      ("set" . "clojure.set")
      ("str" . "clojure.string")
      ("walk" . "clojure.walk")
      ("edn" . "clojure.edn")
      ("zip" . "clojure.zip")
      ("async" . "clojure.core.async")
      ("s" . "clojure.spec.alpha")
      ("instant" . "clojure.instant")
      ("log" . "clojure.tools.logging")
      ("cheshire" . "cheshire.core")
      ("ig" . "integrant.core")
      ("jdbc" . "next.jdbc")
      ("order" . "kaddy.specs.order")
      ("customer" . "kaddy.specs.customer")
      ("supplier" . "kaddy.specs.supplier")
      ("contact" . "kaddy.specs.contact")
      ("address" . "kaddy.specs.address")
      ("product" . "kaddy.specs.product")
      ("shopify" . "kaddy.specs.shopify")
      ("incentive" . "kaddy.specs.incentive")
      ("line-item" . "kaddy.specs.line-item")
      ("region" . "kaddy.specs.region")
      ("user" . "kaddy.specs.user")
      ("supplier-region" . "kaddy.specs.supplier.region")
      ("debit-account" . "kaddy.specs.debit-account")
      ("split-contact" . "kaddy.specs.split.contact")
      ("user" . "kaddy.specs.user")
      ("ks" . "kaddy.specs")
      ("m-order" . "kaddy.model.order")
      ("m-customer" . "kaddy.model.customer")
      ("m-supplier" . "kaddy.model.supplier")
      ("m-contact" . "kaddy.model.contact")
      ("m-address" . "kaddy.model.address")
      ("db" . "kaddy.db")
      ("misc" . "kaddy.misc")
      ("math" . "kaddy.math")
      ("env" . "kaddy.env")
      ("fulcro-util" . "kaddy.app.fulcro-util")
      ("tempid" . "com.fulcrologic.fulcro.algorithms.tempid")
      ("fdn" . "com.fulcrologic.fulcro.algorithms.denormalize")
      ("comp" . "com.fulcrologic.fulcro.components")
      ("dom" . "com.fulcrologic.fulcro.dom")
      ("fs" . "com.fulcrologic.fulcro.algorithms.form-state")
      ("df" . "com.fulcrologic.fulcro.data-fetch")
      ("dr" . "com.fulcrologic.fulcro.routing.dynamic-routing")
      ("m" . "com.fulcrologic.fulcro.mutations")
      ("uism" . "com.fulcrologic.fulcro.ui-state-machines")
      ("app" . "com.fulcrologic.fulcro.application"))
     (cljr-magic-require-namespaces
      ("io" . "clojure.java.io")
      ("set" . "clojure.set")
      ("str" . "clojure.string")
      ("walk" . "clojure.walk")
      ("edn" . "clojure.edn")
      ("zip" . "clojure.zip")
      ("async" . "clojure.core.async")
      ("s" . "clojure.spec.alpha")
      ("instant" . "clojure.instant")
      ("log" . "clojure.tools.logging")
      ("cheshire" . "cheshire.core")
      ("ig" . "integrant.core")
      ("jdbc" . "next.jdbc")
      ("order" . "kaddy.specs.order")
      ("customer" . "kaddy.specs.customer")
      ("supplier" . "kaddy.specs.supplier")
      ("contact" . "kaddy.specs.contact")
      ("address" . "kaddy.specs.address")
      ("product" . "kaddy.specs.product")
      ("shopify" . "kaddy.specs.shopify")
      ("incentive" . "kaddy.specs.incentive")
      ("line-item" . "kaddy.specs.line-item")
      ("region" . "kaddy.specs.region")
      ("user" . "kaddy.specs.user")
      ("supplier-region" . "kaddy.specs.supplier.region")
      ("debit-account" . "kaddy.specs.debit-account")
      ("split-contact" . "kaddy.specs.split.contact")
      ("ks" . "kaddy.specs")
      ("m-order" . "kaddy.model.order")
      ("m-customer" . "kaddy.model.customer")
      ("m-supplier" . "kaddy.model.supplier")
      ("m-contact" . "kaddy.model.contact")
      ("m-address" . "kaddy.model.address")
      ("db" . "kaddy.db")
      ("misc" . "kaddy.misc")
      ("math" . "kaddy.math")
      ("env" . "kaddy.env")
      ("fulcro-util" . "kaddy.app.fulcro-util")
      ("tempid" . "com.fulcrologic.fulcro.algorithms.tempid")
      ("fdn" . "com.fulcrologic.fulcro.algorithms.denormalize")
      ("comp" . "com.fulcrologic.fulcro.components")
      ("dom" . "com.fulcrologic.fulcro.dom")
      ("fs" . "com.fulcrologic.fulcro.algorithms.form-state")
      ("df" . "com.fulcrologic.fulcro.data-fetch")
      ("dr" . "com.fulcrologic.fulcro.routing.dynamic-routing")
      ("m" . "com.fulcrologic.fulcro.mutations")
      ("uism" . "com.fulcrologic.fulcro.ui-state-machines")
      ("app" . "com.fulcrologic.fulcro.application"))
     (cljr-magic-require-namespaces
      ("io" . "clojure.java.io")
      ("set" . "clojure.set")
      ("str" . "clojure.string")
      ("walk" . "clojure.walk")
      ("edn" . "clojure.edn")
      ("zip" . "clojure.zip")
      ("async" . "clojure.core.async")
      ("s" . "clojure.spec.alpha")
      ("instant" . "clojure.instant")
      ("log" . "clojure.tools.logging")
      ("cheshire" . "cheshire.core")
      ("ig" . "integrant.core")
      ("jdbc" . "next.jdbc")
      ("order" . "kaddy.specs.order")
      ("customer" . "kaddy.specs.customer")
      ("supplier" . "kaddy.specs.supplier")
      ("contact" . "kaddy.specs.contact")
      ("address" . "kaddy.specs.address")
      ("product" . "kaddy.specs.product")
      ("shopify" . "kaddy.specs.shopify")
      ("incentive" . "kaddy.specs.incentive")
      ("line-item" . "kaddy.specs.line-item")
      ("region" . "kaddy.specs.region")
      ("user" . "kaddy.specs.user")
      ("supplier-region" . "kaddy.specs.supplier.region")
      ("direct-account" . "kaddy.specs.debit-account")
      ("split-contact" . "kaddy.specs.split.contact")
      ("ks" . "kaddy.specs")
      ("m-order" . "kaddy.model.order")
      ("m-customer" . "kaddy.model.customer")
      ("m-supplier" . "kaddy.model.supplier")
      ("m-contact" . "kaddy.model.contact")
      ("m-address" . "kaddy.model.address")
      ("db" . "kaddy.db")
      ("misc" . "kaddy.misc")
      ("math" . "kaddy.math")
      ("env" . "kaddy.env")
      ("fulcro-util" . "kaddy.app.fulcro-util")
      ("tempid" . "com.fulcrologic.fulcro.algorithms.tempid")
      ("fdn" . "com.fulcrologic.fulcro.algorithms.denormalize")
      ("comp" . "com.fulcrologic.fulcro.components")
      ("dom" . "com.fulcrologic.fulcro.dom")
      ("fs" . "com.fulcrologic.fulcro.algorithms.form-state")
      ("df" . "com.fulcrologic.fulcro.data-fetch")
      ("dr" . "com.fulcrologic.fulcro.routing.dynamic-routing")
      ("m" . "com.fulcrologic.fulcro.mutations")
      ("uism" . "com.fulcrologic.fulcro.ui-state-machines")
      ("app" . "com.fulcrologic.fulcro.application"))
     (cljr-magic-require-namespaces
      ("io" . "clojure.java.io")
      ("set" . "clojure.set")
      ("str" . "clojure.string")
      ("walk" . "clojure.walk")
      ("edn" . "clojure.edn")
      ("zip" . "clojure.zip")
      ("async" . "clojure.core.async")
      ("s" . "clojure.spec.alpha")
      ("instant" . "clojure.instant")
      ("log" . "clojure.tools.logging")
      ("cheshire" . "cheshire.core")
      ("ig" . "integrant.core")
      ("jdbc" . "next.jdbc")
      ("order" . "kaddy.specs.order")
      ("customer" . "kaddy.specs.customer")
      ("supplier" . "kaddy.specs.supplier")
      ("contact" . "kaddy.specs.contact")
      ("address" . "kaddy.specs.address")
      ("product" . "kaddy.specs.product")
      ("shopify" . "kaddy.specs.shopify")
      ("incentive" . "kaddy.specs.incentive")
      ("line-item" . "kaddy.specs.line-item")
      ("region" . "kaddy.specs.region")
      ("user" . "kaddy.specs.user")
      ("supplier-region" . "kaddy.specs.supplier.region")
      ("ks" . "kaddy.specs")
      ("m-order" . "kaddy.model.order")
      ("m-customer" . "kaddy.model.customer")
      ("m-supplier" . "kaddy.model.supplier")
      ("m-contact" . "kaddy.model.contact")
      ("m-address" . "kaddy.model.address")
      ("db" . "kaddy.db")
      ("misc" . "kaddy.misc")
      ("math" . "kaddy.math")
      ("env" . "kaddy.env")
      ("fulcro-util" . "kaddy.app.fulcro-util")
      ("tempid" . "com.fulcrologic.fulcro.algorithms.tempid")
      ("fdn" . "com.fulcrologic.fulcro.algorithms.denormalize")
      ("comp" . "com.fulcrologic.fulcro.components")
      ("dom" . "com.fulcrologic.fulcro.dom")
      ("fs" . "com.fulcrologic.fulcro.algorithms.form-state")
      ("df" . "com.fulcrologic.fulcro.data-fetch")
      ("dr" . "com.fulcrologic.fulcro.routing.dynamic-routing")
      ("m" . "com.fulcrologic.fulcro.mutations")
      ("uism" . "com.fulcrologic.fulcro.ui-state-machines")
      ("app" . "com.fulcrologic.fulcro.application"))
     (cljr-magic-require-namespaces
      ("io" . "clojure.java.io")
      ("set" . "clojure.set")
      ("str" . "clojure.string")
      ("walk" . "clojure.walk")
      ("edn" . "clojure.edn")
      ("zip" . "clojure.zip")
      ("async" . "clojure.core.async")
      ("s" . "clojure.spec.alpha")
      ("instant" . "clojure.instant")
      ("log" . "clojure.tools.logging")
      ("cheshire" . "cheshire.core")
      ("ig" . "integrant.core")
      ("jdbc" . "next.jdbc")
      ("order" . "kaddy.specs.order")
      ("customer" . "kaddy.specs.customer")
      ("supplier" . "kaddy.specs.supplier")
      ("contact" . "kaddy.specs.contact")
      ("address" . "kaddy.specs.address")
      ("product" . "kaddy.specs.product")
      ("shopify" . "kaddy.specs.shopify")
      ("incentive" . "kaddy.specs.incentive")
      ("line-item" . "kaddy.specs.line-item")
      ("region" . "kaddy.specs.region")
      ("supplier-region" . "kaddy.specs.supplier.region")
      ("user" . "kaddy.specs.user")
      ("ks" . "kaddy.specs")
      ("m-order" . "kaddy.model.order")
      ("m-customer" . "kaddy.model.customer")
      ("m-supplier" . "kaddy.model.supplier")
      ("m-contact" . "kaddy.model.contact")
      ("m-address" . "kaddy.model.address")
      ("db" . "kaddy.db")
      ("misc" . "kaddy.misc")
      ("math" . "kaddy.math")
      ("env" . "kaddy.env")
      ("fulcro-util" . "kaddy.app.fulcro-util")
      ("tempid" . "com.fulcrologic.fulcro.algorithms.tempid")
      ("fdn" . "com.fulcrologic.fulcro.algorithms.denormalize")
      ("comp" . "com.fulcrologic.fulcro.components")
      ("dom" . "com.fulcrologic.fulcro.dom")
      ("fs" . "com.fulcrologic.fulcro.algorithms.form-state")
      ("df" . "com.fulcrologic.fulcro.data-fetch")
      ("dr" . "com.fulcrologic.fulcro.routing.dynamic-routing")
      ("m" . "com.fulcrologic.fulcro.mutations")
      ("uism" . "com.fulcrologic.fulcro.ui-state-machines")
      ("app" . "com.fulcrologic.fulcro.application"))
     (cljr-magic-require-namespaces
      ("io" . "clojure.java.io")
      ("set" . "clojure.set")
      ("str" . "clojure.string")
      ("walk" . "clojure.walk")
      ("edn" . "clojure.edn")
      ("zip" . "clojure.zip")
      ("async" . "clojure.core.async")
      ("s" . "clojure.spec.alpha")
      ("instant" . "clojure.instant")
      ("log" . "clojure.tools.logging")
      ("cheshire" . "cheshire.core")
      ("ig" . "integrant.core")
      ("jdbc" . "next.jdbc")
      ("order" . "kaddy.specs.order")
      ("customer" . "kaddy.specs.customer")
      ("supplier" . "kaddy.specs.supplier")
      ("contact" . "kaddy.specs.contact")
      ("address" . "kaddy.specs.address")
      ("product" . "kaddy.specs.product")
      ("shopify" . "kaddy.specs.shopify")
      ("incentive" . "kaddy.specs.incentive")
      ("line-item" . "kaddy.specs.line-item")
      ("region" . "kaddy.specs.region")
      ("supplier-region" . "kaddy.specs.supplier.region")
      ("ks" . "kaddy.specs")
      ("m-order" . "kaddy.model.order")
      ("m-customer" . "kaddy.model.customer")
      ("m-supplier" . "kaddy.model.supplier")
      ("m-contact" . "kaddy.model.contact")
      ("m-address" . "kaddy.model.address")
      ("db" . "kaddy.db")
      ("misc" . "kaddy.misc")
      ("math" . "kaddy.math")
      ("env" . "kaddy.env")
      ("fulcro-util" . "kaddy.app.fulcro-util")
      ("tempid" . "com.fulcrologic.fulcro.algorithms.tempid")
      ("fdn" . "com.fulcrologic.fulcro.algorithms.denormalize")
      ("comp" . "com.fulcrologic.fulcro.components")
      ("dom" . "com.fulcrologic.fulcro.dom")
      ("fs" . "com.fulcrologic.fulcro.algorithms.form-state")
      ("df" . "com.fulcrologic.fulcro.data-fetch")
      ("dr" . "com.fulcrologic.fulcro.routing.dynamic-routing")
      ("m" . "com.fulcrologic.fulcro.mutations")
      ("uism" . "com.fulcrologic.fulcro.ui-state-machines")
      ("app" . "com.fulcrologic.fulcro.application"))
     (cider-offer-to-open-cljs-app-in-browser . false)
     (cljr-magic-require-namespaces
      ("io" . "clojure.java.io")
      ("set" . "clojure.set")
      ("str" . "clojure.string")
      ("walk" . "clojure.walk")
      ("edn" . "clojure.edn")
      ("zip" . "clojure.zip")
      ("async" . "clojure.core.async")
      ("s" . "clojure.spec.alpha")
      ("instant" . "clojure.instant")
      ("log" . "clojure.tools.logging")
      ("cheshire" . "cheshire.core")
      ("ig" . "integrant.core")
      ("jdbc" . "next.jdbc")
      ("order" . "kaddy.specs.order")
      ("customer" . "kaddy.specs.customer")
      ("supplier" . "kaddy.specs.supplier")
      ("contact" . "kaddy.specs.contact")
      ("address" . "kaddy.specs.address")
      ("product" . "kaddy.specs.product")
      ("shopify" . "kaddy.specs.shopify")
      ("incentive" . "kaddy.specs.incentive")
      ("line-item" . "kaddy.specs.line-item")
      ("ks" . "kaddy.specs")
      ("m-order" . "kaddy.model.order")
      ("m-customer" . "kaddy.model.customer")
      ("m-supplier" . "kaddy.model.supplier")
      ("m-contact" . "kaddy.model.contact")
      ("m-address" . "kaddy.model.address")
      ("db" . "kaddy.db")
      ("misc" . "kaddy.misc")
      ("math" . "kaddy.math")
      ("env" . "kaddy.env")
      ("fulcro-util" . "kaddy.app.fulcro-util")
      ("tempid" . "com.fulcrologic.fulcro.algorithms.tempid")
      ("fdn" . "com.fulcrologic.fulcro.algorithms.denormalize")
      ("comp" . "com.fulcrologic.fulcro.components")
      ("dom" . "com.fulcrologic.fulcro.dom")
      ("fs" . "com.fulcrologic.fulcro.algorithms.form-state")
      ("df" . "com.fulcrologic.fulcro.data-fetch")
      ("dr" . "com.fulcrologic.fulcro.routing.dynamic-routing")
      ("m" . "com.fulcrologic.fulcro.mutations")
      ("uism" . "com.fulcrologic.fulcro.ui-state-machines")
      ("app" . "com.fulcrologic.fulcro.application"))
     (eval define-clojure-indent
	   (div
	    '(1))
	   (select
	    '(1))
	   (ui-table
	    '(1))
	   (ui-table-row
	    '(1))
	   (ui-table-body
	    '(1))
	   (ui-table-header
	    '(1))
	   (ui-segment
	    '(1))
	   (ui-list
	    '(1))
	   (ui-list-item
	    '(1))
	   (ui-list-content
	    '(1))
	   (ui-list-header
	    '(1))
	   (ui-modal
	    '(1))
	   (ui-modal-header
	    '(1))
	   (ui-modal-content
	    '(1))
	   (ui-modal-actions
	    '(1))
	   (ui-form
	    '(1))
	   (ui-form-input
	    '(1))
	   (ui-form-select
	    '(1))
	   (table
	    '(1))
	   (>defn
	    '(:defn
	      (1))))
     (cljr-magic-require-namespaces
      ("io" . "clojure.java.io")
      ("set" . "clojure.set")
      ("str" . "clojure.string")
      ("walk" . "clojure.walk")
      ("edn" . "clojure.edn")
      ("zip" . "clojure.zip")
      ("async" . "clojure.core.async")
      ("s" . "clojure.spec.alpha")
      ("instant" . "clojure.instant")
      ("log" . "clojure.tools.logging")
      ("cheshire" . "cheshire.core")
      ("ig" . "integrant.core")
      ("jdbc" . "next.jdbc")
      ("order" . "kaddy.specs.order")
      ("customer" . "kaddy.specs.customer")
      ("supplier" . "kaddy.specs.supplier")
      ("contact" . "kaddy.specs.contact")
      ("address" . "kaddy.specs.address")
      ("product" . "kaddy.specs.product")
      ("shopify" . "kaddy.specs.shopify")
      ("incentive" . "kaddy.specs.incentive")
      ("line-item" . "kaddy.specs.line-item")
      ("ks" . "kaddy.specs")
      ("m-order" . "kaddy.model.order")
      ("m-customer" . "kaddy.model.customer")
      ("m-supplier" . "kaddy.model.supplier")
      ("m-contact" . "kaddy.model.contact")
      ("m-address" . "kaddy.model.address")
      ("db" . "kaddy.db")
      ("misc" . "kaddy.misc")
      ("math" . "kaddy.math")
      ("env" . "kaddy.env")
      ("fulcro-util" . "kaddy.app.fulcro-util")
      ("tempid" . "com.fulcrologic.fulcro.algorithms.tempid")
      ("fdn" . "com.fulcrologic.fulcro.algorithms.denormalize")
      ("comp" . "com.fulcrologic.fulcro.components")
      ("dom" . "com.fulcrologic.fulcro.dom")
      ("fs" . "com.fulcrologic.fulcro.algorithms.form-state")
      ("df" . "com.fulcrologic.fulcro.data-fetch")
      ("dr" . "com.fulcrologic.fulcro.routing.dynamic-routing")
      ("m" . "com.fulcrologic.fulcro.mutations")
      ("app" . "com.fulcrologic.fulcro.application"))
     (eval define-clojure-indent
	   (div
	    '(1))
	   (ui-table
	    '(1))
	   (ui-table-row
	    '(1))
	   (ui-table-body
	    '(1))
	   (ui-table-header
	    '(1))
	   (ui-segment
	    '(1))
	   (ui-list
	    '(1))
	   (ui-list-item
	    '(1))
	   (ui-list-content
	    '(1))
	   (ui-list-header
	    '(1))
	   (ui-modal
	    '(1))
	   (ui-modal-header
	    '(1))
	   (ui-modal-content
	    '(1))
	   (ui-modal-actions
	    '(1))
	   (ui-form
	    '(1))
	   (ui-form-input
	    '(1))
	   (ui-form-select
	    '(1))
	   (table
	    '(1))
	   (>defn
	    '(:defn
	      (1))))
     (cider-filter-regexps quote
			   (".*nrepl" "^cider.nrepl"))
     (cider-save-file-on-load . t)
     (eval put-clojure-indent '>defn 2)
     (cider-ns-refresh-after-fn . "integrant.repl/resume")
     (cider-ns-refresh-before-fn . "integrant.repl/suspend")
     (checkdoc-package-keywords-flag)))
 '(vc-annotate-background "#282c34")
 '(vc-annotate-color-map
   (list
    (cons 20 "#98be65")
    (cons 40 "#b4be6c")
    (cons 60 "#d0be73")
    (cons 80 "#ECBE7B")
    (cons 100 "#e6ab6a")
    (cons 120 "#e09859")
    (cons 140 "#da8548")
    (cons 160 "#d38079")
    (cons 180 "#cc7cab")
    (cons 200 "#c678dd")
    (cons 220 "#d974b7")
    (cons 240 "#ec7091")
    (cons 260 "#ff6c6b")
    (cons 280 "#cf6162")
    (cons 300 "#9f585a")
    (cons 320 "#6f4e52")
    (cons 340 "#5B6268")
    (cons 360 "#5B6268")))
 '(vc-annotate-very-old-color nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(whitespace-tab ((t (:foreground "#636363")))))
(put 'downcase-region 'disabled nil)
