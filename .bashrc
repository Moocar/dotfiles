# Terminal colors
        RED="\[\033[0;31m\]"
     ORANGE="\[\033[0;33m\]"
     YELLOW="\[\033[0;33m\]"
      GREEN="\[\033[0;32m\]"
       BLUE="\[\033[0;34m\]"
  LIGHT_RED="\[\033[1;31m\]"
LIGHT_GREEN="\[\033[1;32m\]"
      WHITE="\[\033[1;37m\]"
 LIGHT_GRAY="\[\033[0;37m\]"
 COLOR_NONE="\[\e[0m\]"

## Fix Bluetooth Audio chopiness
## http://apple.stackexchange.com/a/179209

# defaults write com.apple.BluetoothAudioAgent "Apple Bitpool Min (editable)" -int 40

# defaults write com.apple.BluetoothAudioAgent "Apple Bitpool Max (editable)" 80
# defaults write com.apple.BluetoothAudioAgent "Apple Bitpool Min (editable)" 48
# defaults write com.apple.BluetoothAudioAgent "Apple Initial Bitpool (editable)" 40
# defaults write com.apple.BluetoothAudioAgent "Apple Initial Bitpool Min (editable)" 40
# defaults write com.apple.BluetoothAudioAgent "Negotiated Bitpool" 58
# defaults write com.apple.BluetoothAudioAgent "Negotiated Bitpool Max" 58
# defaults write com.apple.BluetoothAudioAgent "Negotiated Bitpool Min" 48

# sudo killall bluetoothaudiod

 # Used later (maybe in .bash_local) to determine if this is an
# interactive shell; see http://www.gnu.org/software/bash/manual/html_node/Is-this-Shell-Interactive_003f.html
case "$-" in
*i*)    INTERACTIVE_SHELL="true" ;;
*)      : ;;
esac

# Basic environment
export TERM=xterm-256color
export PS1="${BLUE}\W \$${COLOR_NONE} "
# export EDITOR=/usr/bin/nano

# PATH munging
# See http://tldp.org/LDP/Bash-Beginners-Guide/html/sect_11_02.html
EGREP=$(which egrep)

# Golang is installed via https://golang.org/doc/install

export GOPATH=~/go
export GOROOT=/usr/local/go

pathmunge () {
    if ! echo $PATH | $EGREP -q "(^|:)$1($|:)" ; then
        if [ "$2" = "after" ] ; then
            PATH=$PATH:$1
        else
            PATH=$1:$PATH
        fi
    fi
}

pathmunge /sbin
pathmunge /usr/sbin
pathmunge /usr/local/sbin
pathmunge /bin
pathmunge /usr/bin
pathmunge /opt/local/bin
export PATH="/usr/local/bin:$PATH"

pathmunge "$HOME/bin"

pathmunge /opt/X11/bin after
pathmunge /usr/texbin after
pathmunge "$HOME/Library/Python/2.7/bin"
pathmunge "${GOPATH//://bin:}/bin"
pathmunge "$GOROOT/bin"
pathmunge "/Library/PostgreSQL/11/bin"

pathmunge "/Applications/CMake.app/Contents/bin"

export PATH
unset pathmunge



# My aliases
if [[ "$(uname)" == "Darwin" ]]; then
    alias l="ls -FG"
    alias ls="ls -FG"
    alias ll="ls -lhFG"
    alias la="ls -ahFG"
    alias lal="ls -lahFG"
    alias d="pwd && echo && ls -FG"
else
    alias l="ls --color -F"
    alias ls="ls --color -F"
    alias ll="ls --color -lhF"
    alias la="ls --color -ahF"
    alias lal="ls --color -lahF"
    alias d="pwd && echo && ls --color -F"
fi

alias beep="echo -e '\a'"

alias pgrep='ps aux | grep'

alias gs='git status'

alias fig='rlwrap lein figwheel'

if [ -e /Applications/TrueCrypt.app/Contents/MacOS/TrueCrypt ]; then
    alias truecrypt="/Applications/TrueCrypt.app/Contents/MacOS/TrueCrypt -t"
fi

# if which open > /dev/null; then
#     alias e="open -a Emacs.app"
# elif which emacs > /dev/null; then
#     alias e="emacs"
# else
#     alias e="nano"
# fi

# alias emacsclient="/Applications/Emacs.app/Contents/MacOS/bin/emacsclient"
# alias emacs="ec"
# export EDITOR="ec"

# Local system stuff
if [ -e ~/.bash_local ]; then
    source ~/.bash_local
fi

# Git autocompletion
if [ -f ~/.git-completion.bash ]; then
    source ~/.git-completion.bash
fi

# Git helper functions
function git-delete-merged-branches() {
    git branch --merged | grep -v "\*" | grep -v master | xargs -n 1 git branch -d
    git remote prune origin
}

# Java on OS X
if [[ -f /usr/libexec/java_home ]]; then
    export JAVA_HOME="$(/usr/libexec/java_home -v 11.0.5)"
fi

## GPG Agent
## From http://sudoers.org/2013/11/05/gpg-agent.html
GPG_AGENT=$(which gpg-agent)
GPG_TTY=`tty`
export GPG_TTY
if [[ -f ${GPG_AGENT} && -e "${HOME}/.bash_gpg" ]]; then
    source "${HOME}/.bash_gpg"
fi

## 'pass' Password Manager; http://www.zx2c4.com/projects/password-store/
if [[ -e /usr/local/etc/bash_completion.d/password-store ]]; then
    source /usr/local/etc/bash_completion.d/password-store
fi

if [ -d ~/src/clj/clojurescript ]; then
    export CLOJURESCRIPT_HOME=~/src/clj/clojurescript
fi

#### history ####

HISTSIZE=100000
HISTFILESIZE=100000
HISTIGNORE=exit:l:la:ll:lsd:pwd:..:...:....:.....
HISTCONTROL=ignoredups

# Append to the Bash history file, rather than overwriting it
shopt -s histappend

# Add tab completion for SSH hostnames based on ~/.ssh/config, ignoring wildcards
[ -e "$HOME/.ssh/config" ] && complete -o "default" -o "nospace" -W "$(grep "^Host" ~/.ssh/config | grep -v "[?*]" | cut -d " " -f2 | tr ' ' '\n')" scp sftp ssh

# enable ctrl-s to be used as forward search in terminal on mac
stty -ixon



# MacPorts Installer addition on 2016-11-25_at_16:04:44: adding an appropriate PATH variable for use with MacPorts.
# export PATH="/opt/local/bin:/opt/local/sbin:$PATH"
# Finished adapting your PATH environment variable for use with MacPorts.


# The next line updates PATH for the Google Cloud SDK.
if [ -f '/usr/local/google-cloud-sdk/path.bash.inc' ]; then source '/usr/local/google-cloud-sdk/path.bash.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/usr/local/google-cloud-sdk/completion.bash.inc' ]; then source '/usr/local/google-cloud-sdk/completion.bash.inc'; fi

# alias java="java8"

export PATH="/usr/local/opt/maven@3.5/bin:$PATH"

# export GOPATH=$(go env GOPATH)

eval "$(direnv hook bash)"

eval "$(nodenv init -)"

export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"

export NVM_DIR="$HOME/.nvm"
  [ -s "/usr/local/opt/nvm/nvm.sh" ] && . "/usr/local/opt/nvm/nvm.sh"  # This loads nvm
  [ -s "/usr/local/opt/nvm/etc/bash_completion.d/nvm" ] && . "/usr/local/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion

export GRAALVM_HOME="/usr/local/graalvm-ce-java11-19.3.1/Contents/Home"

# Set lower key repeat
# Default is InitialKeyRepeat = 25, KeyRepeat = 2
# heaps more amazing mac options that Matias showed me here: https://github.com/mathiasbynens/dotfiles/blob/master/.macos
defaults write -g InitialKeyRepeat -int 15
defaults write -g KeyRepeat -int 1

## emacs vtermn

vterm_printf(){
    if [ -n "$TMUX" ]; then
        # Tell tmux to pass the escape sequences through
        # (Source: http://permalink.gmane.org/gmane.comp.terminal-emulators.tmux.user/1324)
        printf "\ePtmux;\e\e]%s\007\e\\" "$1"
    elif [ "${TERM%%-*}" = "screen" ]; then
        # GNU screen (screen, screen-256color, screen-256color-bce)
        printf "\eP\e]%s\007\e\\" "$1"
    else
        printf "\e]%s\e\\" "$1"
    fi
}

vterm_prompt_end(){
    vterm_printf "51;A$(whoami)@$(hostname):$(pwd)"
}
PS1=$PS1'\[$(vterm_prompt_end)\]'

[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
